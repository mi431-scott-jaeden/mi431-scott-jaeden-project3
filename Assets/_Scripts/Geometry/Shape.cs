﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sebastian.Geometry
{
    public class Shape
    {
        public List<Vector3> points;
        public MeshFilter meshFilter;
        
        public Shape()
        {
            points = new List<Vector3>();
            //Creating new MeshFilter does not actually create and assign it
            meshFilter = new MeshFilter();
        }
    }
}