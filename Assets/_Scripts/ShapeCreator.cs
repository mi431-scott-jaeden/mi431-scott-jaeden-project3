using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sebastian.Geometry;
using System.Linq;
using UnityEngine.UIElements;
using UnityEditor;

public class ShapeCreator : MonoBehaviour
{
    public MeshFilter meshFilter;
    [HideInInspector]
    public List<Shape> shapes = new List<Shape>();

    public float raiseAmount = 0.5f;
    public float selectionLevel = 0.5f;
    public float alignToGridDegree = 0.25f;
    [HideInInspector]
    public bool showShapesList;
    public float handleRadius = .5f;
    public Material InstantiatedMat;
    public Shader DoubleSidedShader;

    public void UpdateMeshDisplay()
    {
        CompositeShape compShape = new CompositeShape(shapes);
        meshFilter.mesh = compShape.GetMesh();
    }
    public void CreateObjectMesh(int index, ref MeshFilter obj)
    {
        if (shapes[index].points.Count > 2)
        {
            List<int> tris = new List<int>();
            for (int o = 0; o < shapes[index].points.Count; o++)
            {//Iterate through to make triangles for a mesh
                int prevShape = o - 1;
                int nextShape = o + 1;
                if ((o - 1) < 0)
                {
                    prevShape = shapes[index].points.Count - 1;
                }
                if ((o + 1) >= shapes[index].points.Count)
                {
                    nextShape = 0;
                }
                if (checkNext(index, o))
                {//If a point is directly above or below another, check 2 and 4 ahead to see if there's another point of the same height
                    int[] places = new int[6];
                    places[0] = o;
                    places[1] = nextShape;
                    for (int i = 2; i < places.Length; i++)
                    {
                        if ((o + i) >= shapes[index].points.Count)
                        {
                            places[i] = (o + i) % (shapes[index].points.Count);
                            Debug.Log(places[i]);
                        }
                    }    

                    if (shapes[index].points[o].y == shapes[index].points[places[5]].y)
                    {
                        if (checkNext(index, places[3]) && checkNext(index, places[5]))
                        {//if the next two sets are above & below each other, draw a line between the low and high sections
                            tris.Add(places[0]);
                            tris.Add(places[2]);
                            tris.Add(places[4]);
                            tris.Add(places[1]);
                            tris.Add(places[3]);
                            tris.Add(places[5]);
                        }
                    }
                }
                int twoNext = ((nextShape + 1) < (shapes[index].points.Count - 1)) ? (nextShape + 1) : 0;
                if (shapes[index].points[twoNext].y != shapes[index].points[o].y)
                {//Make sure the sides of the mesh are filled
                    tris.Add(o);
                    tris.Add(nextShape);
                    tris.Add(twoNext);
                }
                tris.Add(nextShape);
                tris.Add(o);
                tris.Add(prevShape);
            }
            obj.mesh = 
                new Mesh()
                {
                    vertices = shapes[index].points.ToArray(),
                    triangles = tris.ToArray(),
                    normals = shapes[index].points.Select(x => Vector3.up).ToArray()
                };

        }
    }
    bool checkNext(int index, int pointIndex)
    {//returns whether a point is directly above or below the next point
        int nextIndex = pointIndex;
        if (pointIndex + 1 > shapes[index].points.Count - 1)
        {
            nextIndex = 0;
        }
        else
        {
            pointIndex += 1;
        }

        Vector3 oldShape = shapes[index].points[pointIndex];
        Vector3 newShape = shapes[index].points[nextIndex];

        if (oldShape.y != newShape.y)
        {
            if (oldShape.x == newShape.x)
            {
                if (oldShape.z == newShape.z)
                {
                    return true;
                }
                else return false;

            }
            else
                return false;
        }
        else
            return false;
    }
}