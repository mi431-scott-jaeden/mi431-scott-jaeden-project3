using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Sebastian.Geometry;
using System.Drawing.Printing;
using JetBrains.Annotations;

[CustomEditor(typeof(ShapeCreator))]
public class ShapeEditor : Editor
{
    ShapeCreator shapeCreator;
    SelectionInfo selectionInfo;
    bool shapeChangedSinceLastRepaint;
    private bool AlignToGrid;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        string helpMessage = "Left Click to add points. \nShift-left Click on point to delete.\nShift-left click on empty space to create new shape.";
        EditorGUILayout.HelpBox(helpMessage, MessageType.Info);
        int shapeDeleteIndex = -1;
        int shapeInstantiateIndex = -1;
        
        if (GUILayout.Button("Snap To Grid"))
        {
            if (AlignToGrid)
            {
                AlignToGrid = false;
            }
            else
            {
                AlignToGrid = true;
            }
        }
        shapeCreator.showShapesList = EditorGUILayout.Foldout(shapeCreator.showShapesList, "Show Shapes List");
        if (shapeCreator.showShapesList)
        {
            for (int i = 0; i < shapeCreator.shapes.Count; i++)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label("Shape " + +(i + 1));

                GUI.enabled = i != selectionInfo.selectedShapeIndex;
                if (GUILayout.Button("Select"))
                {
                    selectionInfo.selectedShapeIndex = i;
                }
                GUI.enabled = true;
                if (GUILayout.Button("Delete"))
                {
                    shapeDeleteIndex = i;
                }
                GUILayout.EndHorizontal();
                if (GUILayout.Button("Instantiate Shape"))
                {
                    shapeInstantiateIndex = i;
                }
            }

        }
        if (shapeDeleteIndex != -1)
        {
            Undo.RecordObject(shapeCreator, "Delete shape");
            shapeCreator.shapes.RemoveAt(shapeDeleteIndex);
            selectionInfo.selectedShapeIndex = Mathf.Clamp(selectionInfo.selectedShapeIndex, 0, shapeCreator.shapes.Count - 1);
        }

        if (shapeInstantiateIndex != -1)
        {
            InstantiateShape(shapeInstantiateIndex);
        }

        if (GUI.changed)
        {
            shapeChangedSinceLastRepaint = true;
            SceneView.RepaintAll();
        }
    }

    void OnSceneGUI()
    {
        Event guiEvent = Event.current;

        if (guiEvent.type == EventType.Repaint)
        {
            Draw();
        }
        else if (guiEvent.type == EventType.Layout)
        {
            HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        }
        else
        {
            HandleInput(guiEvent);
            if (shapeChangedSinceLastRepaint)
            {
                HandleUtility.Repaint();
            }
        }
    }
    
    void CreateNewShape()
    {
        Undo.RecordObject(shapeCreator, "Create shape");
        shapeCreator.shapes.Add(new Shape());
        selectionInfo.selectedShapeIndex = shapeCreator.shapes.Count - 1;
    }

    void CreateNewPoint(Vector3 position)
    {
        bool mouseISOverSelectedShape = selectionInfo.mouseOverShapeIndex == selectionInfo.selectedShapeIndex;
        int newPointIndex = (selectionInfo.mouseIsOverLine && mouseISOverSelectedShape) ? selectionInfo.lineIndex + 1 : SelectedShape.points.Count;
        Undo.RecordObject(shapeCreator, "Add Point");
        SelectedShape.points.Insert(newPointIndex, position);
        selectionInfo.pointIndex = newPointIndex;
        selectionInfo.mouseOverShapeIndex = selectionInfo.selectedShapeIndex;
        shapeChangedSinceLastRepaint = true;
    }

    void CreateNewPoint(Vector3 position, int listPos)
    {
        bool mouseISOverSelectedShape = selectionInfo.mouseOverShapeIndex == selectionInfo.selectedShapeIndex;
        int newPointIndex = (selectionInfo.mouseIsOverLine && mouseISOverSelectedShape) ? selectionInfo.lineIndex + 1 : SelectedShape.points.Count;
        Undo.RecordObject(shapeCreator, "Add Point");
        SelectedShape.points.Insert(listPos, position);
        selectionInfo.pointIndex = newPointIndex;
        selectionInfo.mouseOverShapeIndex = selectionInfo.selectedShapeIndex;
        shapeChangedSinceLastRepaint = true;
    }

    void DeletePointUnderMouse()
    {
        Undo.RecordObject(shapeCreator, "Delete point");
        SelectedShape.points.RemoveAt(selectionInfo.pointIndex);
        selectionInfo.pointIsSelected= false;
        selectionInfo.mouseIsOverPoint = false;
        shapeChangedSinceLastRepaint = true;
    }

    void SelectPointUnderMouse()
    {
        selectionInfo.pointIsSelected = true;
        selectionInfo.mouseIsOverPoint = true;
        selectionInfo.mouseIsOverLine = false;
        selectionInfo.lineIndex = -1;

        selectionInfo.positionAtStartOfDrag = SelectedShape.points[selectionInfo.pointIndex];
        shapeChangedSinceLastRepaint = true;
    }

    void SelectShapeUnderMouse()
    {
        if (selectionInfo.mouseOverShapeIndex != -1)
        {
            selectionInfo.selectedShapeIndex = selectionInfo.mouseOverShapeIndex;
            shapeChangedSinceLastRepaint = true;
        }
    }

    void HandleInput(Event guiEvent)
    {
        Ray mouseRay = HandleUtility.GUIPointToWorldRay(guiEvent.mousePosition);
        float drawPlaneHeight = shapeCreator.selectionLevel;
        float dstToDrawPlane = (drawPlaneHeight - mouseRay.origin.y) / mouseRay.direction.y;
        Vector3 mousePosition = mouseRay.GetPoint(dstToDrawPlane);

        if (guiEvent.type == EventType.MouseDown && guiEvent.button == 0 && guiEvent.modifiers == EventModifiers.Shift)
        {
            HandleShiftLeftMouseDown(mousePosition);
        }
        if (guiEvent.type == EventType.KeyDown && guiEvent.keyCode == KeyCode.W)
        {
            RaisePoint();
        }
        if (guiEvent.type == EventType.MouseDown && guiEvent.button == 0 && guiEvent.modifiers == EventModifiers.None)
        {
            HandleLeftMouseDown(mousePosition);
        }
        if (guiEvent.type == EventType.MouseUp && guiEvent.button == 0)
        {
            HandleLeftMouseUp(mousePosition);
        }
        if(guiEvent.type == EventType.MouseDrag && guiEvent.button == 0 && guiEvent.modifiers == EventModifiers.None)
        {
            HandleLeftMouseDrag(mousePosition);
        }
        if (!selectionInfo.pointIsSelected)
            UpdateMouseInfo(mousePosition);
    }

    void HandleShiftLeftMouseDown(Vector3 mousePosition)
    {
        if (selectionInfo.mouseIsOverPoint)
        {
            SelectShapeUnderMouse();
            DeletePointUnderMouse();
        }
        else
        {
            CreateNewShape();
            CreateNewPoint(SnapMouse(mousePosition));
        }
    }

    void HandleLeftMouseDown(Vector3 mousePosition)
    {
        if (shapeCreator.shapes.Count== 0)
        {
            CreateNewShape();
        }
        SelectShapeUnderMouse();
        if (selectionInfo.mouseIsOverPoint)
        {
            SelectPointUnderMouse();
        }
        else
        {
            CreateNewPoint(SnapMouse(mousePosition));
        }
        selectionInfo.pointIsSelected= true;
        selectionInfo.positionAtStartOfDrag = SnapMouse(mousePosition);
        shapeChangedSinceLastRepaint = true;
    }
    void HandleLeftMouseUp(Vector3 mousePosition)
    {
        if (selectionInfo.pointIsSelected)
        {
            SelectedShape.points[selectionInfo.pointIndex] = selectionInfo.positionAtStartOfDrag;
            Undo.RecordObject(shapeCreator, "Move point");
            SelectedShape.points[selectionInfo.pointIndex] = SnapMouse(mousePosition);

            selectionInfo.pointIsSelected = false;
            selectionInfo.pointIndex = -1;
            shapeChangedSinceLastRepaint= true;
        }
    }
    void HandleLeftMouseDrag(Vector3 mousePosition)
    {
        if (selectionInfo.mouseIsOverPoint)
        {
            SelectPointUnderMouse();
            SelectedShape.points[selectionInfo.pointIndex] = SnapMouse(mousePosition);
            shapeChangedSinceLastRepaint = true;
        }
    }

    void RaisePoint()
    {
        //if(selectionInfo.pointIsSelected)
        {
            CreateNewPoint
            (
                new Vector3(SelectedShape.points[selectionInfo.pointIndex].x,
                SelectedShape.points[selectionInfo.pointIndex].y + shapeCreator.raiseAmount,
                SelectedShape.points[selectionInfo.pointIndex].z), 
                selectionInfo.pointIndex
            );
            shapeChangedSinceLastRepaint = true;
            Draw();
        }
    }

    void UpdateMouseInfo(Vector3 mousePosition)
    {
        int mouseOverPointIndex = -1;
        int mouseOverShapeIndex = -1;
        for (int shapeIndex = 0; shapeIndex < shapeCreator.shapes.Count; shapeIndex++)
        {
            Shape currentShape = shapeCreator.shapes[shapeIndex];

            for (int i = 0; i < currentShape.points.Count; i++)
            {
                if (Vector3.Distance(mousePosition, currentShape.points[i]) < shapeCreator.handleRadius)
                {
                    mouseOverPointIndex = i;
                    mouseOverShapeIndex = shapeIndex;
                    break;
                }
            }
        }
        if (mouseOverPointIndex != selectionInfo.pointIndex || mouseOverShapeIndex != selectionInfo.mouseOverShapeIndex)
        {
            selectionInfo.mouseOverShapeIndex = mouseOverShapeIndex;
            selectionInfo.pointIndex = mouseOverPointIndex;
            selectionInfo.mouseIsOverPoint = mouseOverPointIndex != -1;

            shapeChangedSinceLastRepaint = true;
        }
        if (selectionInfo.mouseIsOverPoint)
        {
            selectionInfo.mouseIsOverLine = false;
            selectionInfo.lineIndex = -1;
        }
        else
        {
            int mouseOverLineIndex = -1;
            float closestLineDst = shapeCreator.handleRadius;
            for (int shapeIndex = 0; shapeIndex < shapeCreator.shapes.Count; shapeIndex++)
            {
                Shape currentShape = shapeCreator.shapes[shapeIndex];

                for (int i = 0; i < currentShape.points.Count; i++)
                {
                    Vector3 nextPointInShape = currentShape.points[(i + 1) % currentShape.points.Count];
                    float dstFromMouseToLine = HandleUtility.DistancePointToLineSegment(mousePosition, currentShape.points[i], nextPointInShape);
                    if (dstFromMouseToLine < closestLineDst)
                    {
                        closestLineDst = dstFromMouseToLine;
                        mouseOverLineIndex = i;
                        mouseOverShapeIndex = shapeIndex;
                    }
                }
            }
            if (selectionInfo.lineIndex != mouseOverLineIndex || mouseOverShapeIndex != selectionInfo.mouseOverShapeIndex)
            {
                selectionInfo.mouseOverShapeIndex = mouseOverShapeIndex;
                selectionInfo.lineIndex = mouseOverLineIndex;
                selectionInfo.mouseIsOverLine= mouseOverLineIndex != -1;
                shapeChangedSinceLastRepaint = true;
            }
        }
    }

    void Draw()
    {
        for (int shapeIndex = 0; shapeIndex < shapeCreator.shapes.Count; shapeIndex++)
        {
            Shape shapeToDraw = shapeCreator.shapes[shapeIndex];
            bool ShapeIsSelected = shapeIndex == selectionInfo.selectedShapeIndex;
            bool mouseIsOverShape = shapeIndex == selectionInfo.mouseOverShapeIndex;
            Color deselectedShapeColour = Color.grey;

            for (int i = 0; i < shapeToDraw.points.Count; i++)
            {
                Vector3 nextPoint = shapeToDraw.points[(i + 1) % shapeToDraw.points.Count];
                if (i == selectionInfo.lineIndex && mouseIsOverShape)
                {
                    Handles.color = Color.red;
                    Handles.DrawLine(shapeToDraw.points[i], nextPoint);
                }
                else
                {
                    Handles.color = (ShapeIsSelected) ? Color.black : deselectedShapeColour;
                    Handles.DrawDottedLine(shapeToDraw.points[i], nextPoint, 4);
                }
                /*if (shapeToDraw.points.Count >= 2)
                    {
                        int twoAhead = ((i + 2) >= shapeToDraw.points.Count) ? (i + 2) % (shapeToDraw.points.Count - 1) : i + 2;

                        if (shapeToDraw.points[i].x == nextPoint.x && shapeToDraw.points[i].x == nextPoint.x)
                        {//Draw a dotted line straight down if applicable
                            Handles.DrawDottedLine(shapeToDraw.points[i], nextPoint, 4);
                        }
                        if (shapeToDraw.points[twoAhead].y != nextPoint.y && shapeToDraw.points[i].y == nextPoint.y)
                        {//Makes sure top points are connected to bottom points without overlapping lines
                            Handles.DrawDottedLine(shapeToDraw.points[i], shapeToDraw.points[twoAhead], 4);
                        }
                        if (shapeToDraw.points[i].y == nextPoint.y)
                        {
                            Handles.DrawDottedLine(shapeToDraw.points[i], nextPoint, 4);
                        }*/
                    if (shapeToDraw.points[i].y != nextPoint.y)
                    {//A special case to connect a previous point to the current one
                        if (i > 0)
                        {
                            Handles.DrawDottedLine(shapeToDraw.points[i - 1], nextPoint, 4);
                        }
                        else
                        {
                            Handles.DrawDottedLine(shapeToDraw.points[shapeToDraw.points.Count - 1], nextPoint, 4);
                        }
                    }
                    //}
                if (i == selectionInfo.pointIndex && mouseIsOverShape)
                {
                    Handles.color = (selectionInfo.pointIsSelected) ? Color.black : Color.red;
                }
                else
                {
                    Handles.color = (ShapeIsSelected)?Color.white : deselectedShapeColour;
                }
                Handles.DrawSolidDisc(shapeToDraw.points[i], Vector3.up, shapeCreator.handleRadius);
            }
        }
        if (shapeChangedSinceLastRepaint)
        {
            shapeCreator.UpdateMeshDisplay();
        }
        shapeChangedSinceLastRepaint = false;
    }

    void InstantiateShape(int shapeIndex)
    {
        GameObject createdObject = new GameObject("Block");
        MeshRenderer rend;
        MeshFilter filt;
        rend = createdObject.AddComponent<MeshRenderer>();
        filt = createdObject.AddComponent<MeshFilter>();
        rend.material = shapeCreator.InstantiatedMat;
        rend.sharedMaterial.shader = shapeCreator.DoubleSidedShader;
        shapeCreator.CreateObjectMesh(shapeIndex, ref filt);
        createdObject.AddComponent<MeshCollider>();
    }

    void OnEnable()
    {
        shapeChangedSinceLastRepaint = true;
        shapeCreator = target as ShapeCreator;
        selectionInfo = new SelectionInfo();
        Undo.undoRedoPerformed += OnUndoOrRedo;
        Tools.hidden = true;
    }

    void OnDisable()
    {
        Undo.undoRedoPerformed -= OnUndoOrRedo;
        Tools.hidden = false;
    }

    Vector3 SnapMouse(Vector3 mousePosition)
    {
        Vector3 returnVal = mousePosition;
        if (AlignToGrid)
        {
            float gridAlignDegree = shapeCreator.alignToGridDegree;
            float remainingX = mousePosition.x % gridAlignDegree;
            float remainingZ = mousePosition.z % gridAlignDegree;

            float directionX;
            float directionZ;

            directionX = Mathf.Round(remainingX / gridAlignDegree);
            directionZ = Mathf.Round(remainingZ / gridAlignDegree);

            if (directionX >= 1)
            {//Subtract the extra then add the step to get to the next degree
                returnVal.x -= remainingX;
                returnVal.x += gridAlignDegree;
            }
            else
            {//Subtract the extra if it's closer to the last position to bring it back to it
                returnVal.x -= remainingX;
            }
            if (directionZ >= 1)
            {//Subtract the extra then add the step
                returnVal.z -= remainingZ;
                returnVal.z += gridAlignDegree;
            }
            else
            {//Subtract the extra if it's closer to the last position
                returnVal.z -= remainingZ;
            }
        }
        return returnVal;
    }

    void OnUndoOrRedo()
    {
        if (selectionInfo.selectedShapeIndex >= shapeCreator.shapes.Count || selectionInfo.selectedShapeIndex == -1)
        {
            selectionInfo.selectedShapeIndex = shapeCreator.shapes.Count - 1;
        }
        shapeChangedSinceLastRepaint = true;
    }

    Shape SelectedShape
    {
        get
        {
            return shapeCreator.shapes[selectionInfo.selectedShapeIndex];
        }
    }

    public class SelectionInfo
    {
        public int selectedShapeIndex;
        public int mouseOverShapeIndex;

        public int pointIndex = -1;
        public bool mouseIsOverPoint;
        public bool pointIsSelected;
        public Vector3 positionAtStartOfDrag;

        public int lineIndex = -1;
        public bool mouseIsOverLine;
    }
}
